# Contributing to ep_unoconv

Please, post any issue to <https://git.framasoft.org/framasoft/ep_unoconv/issues>.

For merge requests, use <https://git.framasoft.org/framasoft/ep_unoconv/merge_requests>.

Any contribution posted to Github will not be considered as Github is only a mirror.

# Coding style

* no tabulation, indent with four spaces
* opening braces have to be on the line of the control statement while the ending braces will have the same indentation as the statement (see `K&R` style on <https://en.wikipedia.org/wiki/Indent_style#Placement_of_braces>)
* no one-liner, ie no `if (true === true) return true;`
* always end your lines with `;`, ie no

        if (true === true) {
            return true
        }

[![](https://framagit.org/assets/favicon-075eba76312e8421991a0c1f89a89ee81678bcde72319dd3e8047e2a47cd3a42.ico)](https://framagit.org)

![English:](https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Flag_of_the_United_Kingdom.svg/20px-Flag_of_the_United_Kingdom.svg.png) **Framasoft uses GitLab** for the development of its free softwares. Our Github repositories are only mirrors.
If you want to work with us, **fork us on [framagit.org](https://framagit.org)**. (no registration needed, you can sign in with your Github account)

![Français :](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Flag_of_France.svg/20px-Flag_of_France.svg.png) **Framasoft utilise GitLab** pour le développement de ses logiciels libres. Nos dépôts Github ne sont que des miroirs.
Si vous souhaitez travailler avec nous, **forkez-nous sur [framagit.org](https://framagit.org)**. (l'inscription n'est pas nécessaire, vous pouvez vous connecter avec votre compte Github)
* * *

# EP\_UNOCONV

This plugin will use [Unoconv](http://dag.wiee.rs/home-made/unoconv/) to import/export files into/from Etherpad instead of Abiword.

Unoconv use [LibreOffice](http://www.libreoffice.org/) to do its job.

To install Unoconv:

```
apt-get install unoconv libreoffice-writer
```

Unoconv can optionally import pdf files if the pdf is an [hybrid](https://wiki.documentfoundation.org/Faq/Writer/PDF_Hybrid) PDF and, from my tests, only if you have LibreOffice version 5 or higher.

If you got LibreOffice 5 or higher, install `libreoffice-pdfimport`:

```
apt-get install libreoffice-pdfimport
```

# License

Copyright 2016 Framasoft

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

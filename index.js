var ERR               = require("ep_etherpad-lite/node_modules/async-stacktrace");
var async             = require("ep_etherpad-lite/node_modules/async");
var padManager        = require("ep_etherpad-lite/node/db/PadManager");
var padMessageHandler = require("ep_etherpad-lite/node/handler/PadMessageHandler");
var hasPadAccess      = require("ep_etherpad-lite/node/padaccess");
var exporthtml        = require("ep_etherpad-lite/node/utils/ExportHtml");
var importHtml        = require("ep_etherpad-lite/node/utils/ImportHtml");
var settings          = require("ep_etherpad-lite/node/utils/Settings");
var eejs              = require("ep_etherpad-lite/node/eejs");
var log4js            = require("ep_etherpad-lite/node_modules/log4js");
var formidable        = require('ep_etherpad-lite/node_modules/formidable');
var fs                = require("fs");
var os                = require("os");
var path              = require("path");
var unoconv           = require("unoconv2");

var tempDirectory = "/tmp";

// tempDirectory changes if the operating system is windows
if(os.type().indexOf("Windows") > -1)
{
  tempDirectory = process.env.TEMP;
}

unoconv.listen();

// Change export URLs
exports.eejsBlock_exportColumn = function(hook_name, args, cb) {
    args.content = args.content + eejs.require("ep_unoconv/templates/links.html", {}, module);
    return cb();
};

// Import files
exports.import = function(hook_name, args, callback){
    var srcFile   = args.srcFile;
    var destFile  = args.destFile;

    var fileEnding   = path.extname(srcFile).toLowerCase();
    knownFileEndings = [".doc", ".docx", ".odt"];
    fileEndingKnown  = (knownFileEndings.indexOf(fileEnding) > -1);

    if (fileEndingKnown) {
        unoconv.convert(srcFile, "html", function(err, result) {
            // catch convert errors
            if (err) {
                console.warn("Converting Error:", err);
                return callback("convertFailed", null);
            } else {
                text = result.toString();
                text = text.replace(/(<h\d[^>]*>)/g, "<p>$1");
                text = text.replace(/(<\/h\d>)/g, "$1</p>");
                fs.writeFile(destFile, new Buffer(text), 'utf8', function(err) {
                    if (err) {
                        callback(err, null);
                    }
                    callback(destFile);
                });
            }
        });
    } else {
        return callback();
    }
};

// At startup
exports.registerRoute  = function (hook_name, args, cb) {
    // Export pads
    args.app.get('/p/:pad/:rev?/unoconv/:type', function(req, res, next) {
        var pad   = req.params.pad;
        var type  = req.params.type;
        var types = ["pdf", "odt", "doc"];
        if (types.indexOf(type) == -1) {
            next();
            return;
        }

        res.header("Access-Control-Allow-Origin", "*");

        hasPadAccess(req, res, function() {
            res.attachment(pad + "." + type);
            var html;
            var randNum;
            var srcFile;

            async.series([
                //render the html document
                function(callback) {
                    exporthtml.getPadHTMLDocument(pad, req.params.rev, false, function(err, _html) {
                        if (ERR(err, callback)) {
                            return;
                        }
                        html = _html;
                        callback();
                    });
                },
                //decide what to do with the html export
                function(callback) {
                    randNum = Math.floor(Math.random()*0xFFFFFFFF);
                    srcFile = tempDirectory + "/etherpad_export_" + randNum + ".html";
                    fs.writeFile(srcFile, html, callback);
                },
                //send the convert job to unoconv
                function(callback) {
                    //ensure html can be collected by the garbage collector
                    html = null;
                    unoconv.convert(srcFile, type, function (err, result) {
                        res.send(result);
                        callback();
                    });
                },
                //clean up temporary files
                function(callback) {
                    fs.unlink(srcFile, callback);
                }
            ], function(err) {
                if (err && err != "stop") {
                    ERR(err);
                }
            })
        });
    });
};
